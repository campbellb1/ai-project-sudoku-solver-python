"""
Project 3:	Constraint Propagation
Programmer:	Brandon Campbell (Framework By Dr. Brian Bennett)
Purpose:	The purpose of this program is to solve Sudoku puzzles using a combination of
			Constraint Propagation and a Backtracking Search. Works with text files of numbers (1-9)
			and '.' or '0' characters as blanks.
"""

import sys


class Sudoku:
	"""
		Sudoku class, which models a Sudoku game.

		Based on Peter Norvig's Suggested Sudoku setup
	"""

	def __init__(self):
		"""
			Initialize digits, rows, columns, the grid, squares, units, peers, and values.
		"""
		self.digits   = '123456789'
		self.rows     = 'ABCDEFGHI'
		self.cols     = self.digits
		self.grid     = dict()
		self.squares  = self.cross_product(self.rows, self.cols)
		unitlist = ([self.cross_product(self.rows, c) for c in self.cols] + \
					[self.cross_product(r, self.cols) for r in self.rows] + \
					[self.cross_product(rs, cs) for rs in ('ABC', 'DEF', 'GHI') for cs in ('123', '456', '789')])
		self.units = dict((s, [u for u in unitlist if s in u]) for s in self.squares)
		self.peers = dict((s, set(sum(self.units[s], []))-set([s])) for s in self.squares)
		self.values = dict((s, self.digits) for s in self.squares)

	@staticmethod
	def cross_product(A, B):
		"""
			Return the cross product of A and B
		"""
		return [a+b for a in A for b in B]

	def __str__(self):
		"""
			Convert the grid into a human-readable string
		"""
		output = ''
		width = 2 + max(len(self.grid[s]) for s in self.squares)
		line = '+'.join(['-' * (width * 3)] * 3)
		for r in self.rows:
			output += (''.join((self.grid[r+c] if self.grid[r+c] not in '0.' else '').center(width)+('|' if c in '36' else '') for c in self.digits)) + "\n"
			if r in 'CF': output += line + "\n"
		return output

	def load_file(self, filename):
		"""
			Load the file into the grid dictionary. Note that keys
			are in the form '[A-I][1-9]' (e.g., 'E5').
		"""
		grid = ''
		with open(filename) as f:
			grid = ''.join(f.readlines())
		grid_values = self.grid_values(grid)
		self.grid = grid_values

	def grid_values(self, grid):
		"""
			Convert grid into a dict of {square: char} with '0' or '.' for empties.
		"""
		chars = [c for c in grid if c in self.digits or c in '0.']
		assert len(chars) == 81
		return dict(zip(self.squares, chars))

	def solve(self):
		"""
			Solve the problem by propagation and backtracking.
		"""
		self.search(self.propagate(self.values))

	def propagate(self, values):
		"""
			propagate(self, list of available values for each cell)
			Returns the updated list of "values" each cell could be.
			Purpose: Constrains the state by restricting the available values
			each of the empty cells can take.
		"""

		#Go through every column and row to 'hit' each cell...
		for col in self.cols:
			for row in self.rows:
				for peer in self.peers[row + col]:	#check ALL of the peers of each cell...
					values[row + col] = values[row + col].replace(self.grid[peer], '')	#for every value of each peer, remove it as an option in the current cell's values.
		return values

	def resetConstraints(self, values):
		"""
			resetConstraints(self, list of available values for each cell)
			Returns the updated list of "values" each cell could be.
			Purpose: Resets all of the potential values of each cell to '123456789'
					 This is used in conjunction with placing constraints; can be used
					 immediately before removing a cell and re-propagating constraints to
					 update constraints on a removal.
		"""
		# Go through all of the cells in each row and column.
		for col in self.cols:
			for row in self.rows:
				values[row + col] = '123456789'	# and set the cell's potential values to '123456789'--ALL
		return values;

	def checkSuccess(self):
		"""
			checkSuccess(self)
			Returns the updated list of "values" each cell could be.
			Purpose: Checks to see if all of the cells have a value in them (i.e. not '.' or '0'. If they do,
			then the puzzle is solved, as the constraints put upon the program would not allow this
			to happen unless the value put in each cell was compliant with the constraints of the 'game'.
		"""
		finished = True		# Go ahead and assume Solved until we encounter a 'blank' cell.
		# Go through every column and row...
		for col in self.cols:
			for row in self.rows:
				if self.grid[row + col] == '0' or self.grid[row + col] == '.':	# If we encounter a 'blank'...
					finished = False	# ...we are NOT finished.

		return finished


	def search(self, values):
		"""
			search(self, list of available values for each cell)
			Returns the updated list of "values" each cell could be.
			Purpose: 	First determines which cell we shall start on by finding the one with the "Minimum Remaining Values"
						(or at least one of a multitude of cells with a minimum count of remaining values). Once the
						cell is determined, each of it's potential values is tried on a different recursion path. This
						process is repeated until all of the cells are filled according to the constraint rules of the
						'game'.
		"""
		#MRV Heuristic--------------------
		minimumCount = 9	# Set the current 'minimum count' to the max (which is 9).
		cellToTry = 'A1'	# Go ahead and initialize the cell to try to A1 just in case.
		if self.checkSuccess():	# Check to see if all cells are already filled in
			return self.values	# If so, return the values
		# Go through each column and row to 'hit' each cell
		for col in self.cols:
			for row in self.rows:
				# If the options for possible values is less than our current minimum and the cell is 'blank'...
				if len(values[row + col]) < minimumCount and (self.grid[row + col] == '0' or self.grid[row + col] == '.'):
					minimumCount = len(values[row + col])	# Update the minimum count and...
					cellToTry = row + col					# ...set it as the cell that we'll fill.
		#END MRV HEURISTIC-----------------

		for letter in values[cellToTry]: # For all of the options for values for this particular cell...
			self.grid[cellToTry] = letter	# Set the value
			values = self.propagate(values)	# Then propagate the constraints for this 'move'

			values = self.search(values)	# Go a level deeper to fill another cell.
			if self.checkSuccess():			# If we've filled all of the cells, start going back up.
				return self.values
			self.grid[cellToTry] = '0'		# Otherwise this is a failed branch, undo the 'move'
			values = self.resetConstraints(values)	# And un-propagate all constraints to make sure we remove the consequences of the last 'move'
			values = self.propagate(values)	# Repropagate constraints after removal of last 'move'
		return values

		#THIS WORKS, BUT SLOWLY----------------------NO MRV HEURISTIC
		# while self.grid[self.rows[x] + self.cols[y]] != '0':
		# 	if x < 8:
		# 		x = x + 1
		# 	elif y < 8:
		# 		y = y + 1
		# 		x = 0
		# 	else:
		# 		return values
        #
		# for letter in values[self.rows[x] + self.cols[y]]:
		# 	self.grid[self.rows[x] + self.cols[y]] = letter
		# 	values = self.propagate(values)
        #
		# 	values = self.search(values, x, y)
		# 	if self.checkSuccess():
		# 		return self.values
		# 	self.grid[self.rows[x] + self.cols[y]] = '0'
		# 	values = self.resetConstraints(values)
		# 	values = self.propagate(values)
		# return values


def main():
	s = Sudoku()
	'''
		The loop reads in as many files as you've passed on the command line.
		Example to read two easy files from the command line:
			python project3.py sudoku_easy1.txt sudoku_easy2.txt
	'''
	for x in range(1,len(sys.argv)):
		s.load_file(sys.argv[x])
		print("\n==============================================")
		print(sys.argv[x].center(46))
		print("==============================================\n")
		print(s)
		print("\n----------------------------------------------\n")
		s.solve()
		print(s)

main()
